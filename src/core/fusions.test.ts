import { listFusionFrom, findFusionPath } from './fusions';

describe('listFusionFrom', () => {
    test('list fusion with this materia', () => {
        expect(listFusionFrom('Electrocute')).toEqual([
            {
                from: [
                    'Electrocute',
                    'Tri-Fire (M)'
                ],
                result: 'Libra',
                id: 'Electrocute-Tri-Fire (M)=Libra'
            },
            {
                from: [
                    'Flare',
                    'Electrocute (M)'
                ],
                result: 'Libra',
                id: 'Flare-Electrocute (M)=Libra'
            },
        ])
    })
})

describe('findFusionPath', () => {
    test('can\'t find path to Tri Fire (no fusion exists to that one)', () => {
        expect(findFusionPath('Fire', 'Tri-Fire')).toHaveLength(0)
    })

    test('find at least one path from Fire to Poison', () => {
        expect(findFusionPath('Cure', 'SPR UP')).toEqual([
            [
                {
                    from: [
                        'Cura',
                        'Cure'
                    ],
                    result: 'Silence',
                    id: 'Cura-Cure=Silence'
                },
                {
                    from: [
                        'Dispel',
                        'Silence'
                    ],
                    result: 'SPR UP',
                    id: 'Dispel-Silence=SPR UP'
                },
            ]
        ])
    })
})
