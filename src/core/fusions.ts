import fusionData from './fusions-data';
import type { Fusion } from './fusions-data';

export function listFusionFrom(materia: string): Array<Fusion> {
    return fusionData.filter((fusion) => fusion.from.includes(materia) || fusion.from.includes(`${materia} (M)`));
}

export function listFusionResultingIn(materia: string): Array<Fusion> {
    return fusionData.filter((fusion) => fusion.result === materia);
}

export function findFusionPath(from: string, to: string): Array<Array<Fusion>> {
    const queue: Array<{ history: Array<Fusion>, materia: string }> = [{ history: [], materia: from }]
    const exploredFusionIds: Array<string> = []
    const possiblePaths: Array<Array<Fusion>> = []

    while (queue.length > 0) {
        const current = queue.pop()
        if (current === undefined) {
            throw new Error('Impossible')
        }
        const possibleFromCurrent = listFusionFrom(current.materia).filter((f) => !exploredFusionIds.includes(f.id))

        exploredFusionIds.push(...possibleFromCurrent.map((f) => f.id))

        possibleFromCurrent.forEach((fusion) => {
            const history = [...current.history, fusion]
            if (fusion.result === to) {
                possiblePaths.push(history)
            } else {
                queue.push({ history, materia: fusion.result })
            }
        })
    }

    return possiblePaths
}
