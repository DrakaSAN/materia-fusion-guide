export interface Fusion {
    id: string
    from: Array<string>
    result: string
};

const fusionData: Array<Fusion> = [
    {
        from: [
            'Fire',
            'Blizzard'
        ],
        result: 'Thunder'
    },
    {
        from: [
            'Thunder',
            'Fire'
        ],
        result: 'Blizzard'
    },
    {
        from: [
            'Fira',
            'Blizzara'
        ],
        result: 'Thunder'
    },
    {
        from: [
            'Blizzara',
            'Thundara'
        ],
        result: 'Fire'
    },
    {
        from: [
            'Thundara',
            'Fira'
        ],
        result: 'Blizzard'
    },
    {
        from: [
            'Firaga',
            'Blizzaga'
        ],
        result: 'Thunder'
    },
    {
        from: [
            'Blizzaga',
            'Thundaga'
        ],
        result: 'Fire'
    },
    {
        from: [
            'Thundaga',
            'Firaga'
        ],
        result: 'Blizzard'
    },
    {
        from: [
            'Cure',
            'Cure'
        ],
        result: 'Cure'
    },
    {
        from: [
            'Cura',
            'Cure'
        ],
        result: 'Silence'
    },
    {
        from: [
            'Curaga',
            'Cura'
        ],
        result: 'Death'
    },
    {
        from: [
            'Regen',
            'Cure'
        ],
        result: 'Dash'
    },
    {
        from: [
            'Esuna',
            'Poison'
        ],
        result: 'VIT UP'
    },
    {
        from: [
            'Dispel',
            'Silence'
        ],
        result: 'SPR UP'
    },
    {
        from: [
            'Barrier',
            'Fire'
        ],
        result: 'HP Up'
    },
    {
        from: [
            'MBarrier',
            'Blizzara'
        ],
        result: 'SPR UP'
    },
    {
        from: [
            'Wall',
            'Barrier'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Drain',
            'Drain Blade'
        ],
        result: 'MAG UP'
    },
    {
        from: [
            'Drainra',
            'Firaga'
        ],
        result: 'Drain'
    },
    {
        from: [
            'Drainga',
            'Drain'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Osmose',
            'Osmose Blade'
        ],
        result: 'MAG UP'
    },
    {
        from: [
            'Osmoga',
            'Power Osmose'
        ],
        result: 'SPR UP'
    },
    {
        from: [
            'Poison',
            'Poison Blade'
        ],
        result: 'MAG UP'
    },
    {
        from: [
            'Silence',
            'Poison'
        ],
        result: 'Dash'
    },
    {
        from: [
            'Stop',
            'Stop Blade'
        ],
        result: 'MAG UP'
    },
    {
        from: [
            'Death',
            'Poison'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Dark Fire',
            'Fire'
        ],
        result: 'Poison'
    },
    {
        from: [
            'Dark Blizzard',
            'Blizzard'
        ],
        result: 'Poison'
    },
    {
        from: [
            'Dark Thunder',
            'Thunder'
        ],
        result: 'Poison'
    },
    {
        from: [
            'Dark Fira',
            'Fira'
        ],
        result: 'Esuna'
    },
    {
        from: [
            'Dark Blizzara',
            'Blizzara'
        ],
        result: 'Silence'
    },
    {
        from: [
            'Dark Thundara',
            'Thundara'
        ],
        result: 'Dispel'
    },
    {
        from: [
            'Dark Firaga',
            'Firaga'
        ],
        result: 'Esuna'
    },
    {
        from: [
            'Dark Blizzaga',
            'Blizzaga'
        ],
        result: 'Esuna'
    },
    {
        from: [
            'Dark Thundaga',
            'Thundaga'
        ],
        result: 'Esuna'
    },
    {
        from: [
            'Hell Firaga',
            'Dark Fire'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Hell Blizzaga',
            'Dark Blizzard'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Hell Thundaga',
            'Dark Thunder'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Gravity',
            'Gravity'
        ],
        result: 'Cure'
    },
    {
        from: [
            'Graviga',
            'Gravity'
        ],
        result: 'Dash'
    },
    {
        from: [
            'Quake',
            'Gravity'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Tri-Thundaga',
            'Tri-Thundaga'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Tri-Fire',
            'Tri-Thundaga (M)'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Electrocute',
            'Tri-Fire (M)'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Flare',
            'Electrocute (M)'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Energy',
            'Flare (M)'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Ultima',
            'Tri-Thundaga'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Darkness',
            'Graviga (M)'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Jump',
            'Power Attack'
        ],
        result: 'AP Up'
    },
    {
        from: [
            'High Jump',
            'Firaga Blade'
        ],
        result: 'AP Up'
    },
    {
        from: [
            'Poison Aerial',
            'Poison Twister'
        ],
        result: 'Dash'
    },
    {
        from: [
            'Silence Aerial',
            'High Jump'
        ],
        result: 'Poison Twister'
    },
    {
        from: [
            'Death Jump',
            'Silence Aerial (M)'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Assault Twister',
            'Vital Slash'
        ],
        result: 'AP Up'
    },
    {
        from: [
            'Assault Twister+',
            'Jump'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Poison Twister',
            'Poison Twister'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Stop Twister',
            'Poison Aerial (M)'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Death Twister',
            'Assault Twister+'
        ],
        result: 'Poison Twister'
    },
    {
        from: [
            'Power Attack',
            'Gravity'
        ],
        result: 'Jump'
    },
    {
        from: [
            'Vital Slash',
            'Power Attack'
        ],
        result: 'Graviga'
    },
    {
        from: [
            'Exploder Blade',
            'Quake'
        ],
        result: 'Jump'
    },
    {
        from: [
            'Blast Wave',
            'Graviga (M)'
        ],
        result: 'Assault Twister'
    },
    {
        from: [
            'Poison Blade',
            'Power Attack'
        ],
        result: 'Poison'
    },
    {
        from: [
            'Silence Blade',
            'Assault Twister'
        ],
        result: 'Poison'
    },
    {
        from: [
            'Stop Blade',
            'Stop'
        ],
        result: 'ATK UP'
    },
    {
        from: [
            'Death Blade',
            'Power Attack'
        ],
        result: 'Death'
    },
    {
        from: [
            'Dispel Blade',
            'Elemental Strike'
        ],
        result: 'Poison'
    },
    {
        from: [
            'Fire Blade',
            'Fire'
        ],
        result: 'Power Attack'
    },
    {
        from: [
            'Blizzard Blade',
            'Blizzard'
        ],
        result: 'Power Attack'
    },
    {
        from: [
            'Thunder Blade',
            'Thunder'
        ],
        result: 'Power Attack'
    },
    {
        from: [
            'Fira Blade',
            'Fira'
        ],
        result: 'Jump'
    },
    {
        from: [
            'Blizzara Blade',
            'Blizzara'
        ],
        result: 'Jump'
    },
    {
        from: [
            'Thundara Blade',
            'Thundara'
        ],
        result: 'Jump'
    },
    {
        from: [
            'Firaga Blade',
            'Firaga'
        ],
        result: 'ATK UP'
    },
    {
        from: [
            'Blizzaga Blade',
            'Blizzaga'
        ],
        result: 'ATK UP'
    },
    {
        from: [
            'Thundaga Blade',
            'Thundaga'
        ],
        result: 'ATK UP'
    },
    {
        from: [
            'Drain Blade',
            'Assault Twister'
        ],
        result: 'Drain'
    },
    {
        from: [
            'Power Drain',
            'Osmoga'
        ],
        result: 'ATK UP'
    },
    {
        from: [
            'Aerial Drain',
            'Drainga'
        ],
        result: 'ATK UP'
    },
    {
        from: [
            'Osmose Blade',
            'Drain'
        ],
        result: 'ATK UP'
    },
    {
        from: [
            'Power Osmose',
            'Drainra'
        ],
        result: 'ATK UP'
    },
    {
        from: [
            'Steal',
            'Steal'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Mug',
            'Steal'
        ],
        result: 'Electrocute'
    },
    {
        from: [
            'Gil Toss',
            'Steal'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Goblin Punch',
            'Goblin Punch'
        ],
        result: 'Libra'
    },
    {
        from: [
            'Iron Fist',
            'Goblin Punch'
        ],
        result: 'Dash'
    },
    {
        from: [
            'Magical Punch',
            'Iron Fist (M)'
        ],
        result: 'Libra (M)'
    },
    {
        from: [
            'Hammer Punch',
            'Magical Punch (M)'
        ],
        result: 'Libra (M)'
    },
    {
        from: [
            'Costly Punch',
            'Goblin Punch'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Libra',
            'Octaslash'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Status Strike',
            'Poison Twister'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Status Ward',
            'Stop'
        ],
        result: 'HP Up'
    },
    {
        from: [
            'Elemental Strike',
            'Assault Twister+'
        ],
        result: 'Fire'
    },
    {
        from: [
            'Elemental Ward',
            'Firaga'
        ],
        result: 'VIT UP'
    },
    {
        from: [
            'SP Turbo Magic',
            'SP Turbo Magic'
        ],
        result: 'Libra'
    },
    {
        from: [
            'SP Turbo Attack',
            'SP Turbo Magic'
        ],
        result: 'Electrocute'
    },
    {
        from: [
            'SP Turbo',
            'SP Turbo Magic'
        ],
        result: 'Flare'
    },
    {
        from: [
            'SP Barrier',
            'SP Turbo Magic'
        ],
        result: 'Energy'
    },
    {
        from: [
            'SP Master',
            'SP Turbo Magic'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'HP Up',
            'HP Up'
        ],
        result: 'Libra'
    },
    {
        from: [
            'HP Up+',
            'HP Up'
        ],
        result: 'Dash'
    },
    {
        from: [
            'HP Up++',
            'HP Up'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'MP Up',
            'MP Up'
        ],
        result: 'Libra'
    },
    {
        from: [
            'MP Up+',
            'MP Up'
        ],
        result: 'Dash'
    },
    {
        from: [
            'MP Up++',
            'MP Up'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'AP Up',
            'AP Up'
        ],
        result: 'Libra'
    },
    {
        from: [
            'AP Up+',
            'AP Up'
        ],
        result: 'Dash'
    },
    {
        from: [
            'AP Up++',
            'AP Up'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'ATK UP',
            'ATK UP'
        ],
        result: 'Libra'
    },
    {
        from: [
            'ATK UP+',
            'ATK UP'
        ],
        result: 'Dash'
    },
    {
        from: [
            'ATK UP++',
            'ATK UP'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'VIT UP',
            'VIT UP'
        ],
        result: 'Libra'
    },
    {
        from: [
            'VIT UP+',
            'VIT UP'
        ],
        result: 'Dash'
    },
    {
        from: [
            'VIT UP++',
            'VIT UP'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'MAG UP',
            'MAG UP'
        ],
        result: 'Libra'
    },
    {
        from: [
            'MAG UP+',
            'MAG UP'
        ],
        result: 'Dash'
    },
    {
        from: [
            'MAG UP++',
            'MAG UP'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'SPR UP',
            'SPR UP'
        ],
        result: 'Libra'
    },
    {
        from: [
            'SPR UP+',
            'SPR UP'
        ],
        result: 'Dash'
    },
    {
        from: [
            'SPR UP++',
            'SPR UP'
        ],
        result: 'Octaslash'
    },
    {
        from: [
            'Smart Consumer',
            'Steal'
        ],
        result: 'Dash'
    }
].map((data) => ({
    ...data,
    id: `${data.from.join('-')}=${data.result}`
}));

export default fusionData;
