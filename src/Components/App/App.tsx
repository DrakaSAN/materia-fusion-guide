import React, { useState } from 'react';
import Tab from '../Tab/Tab';
import From from '../From/From';
import To from '../To/To';
import PathFinder from '../PathFinder/PathFinder';
import './App.css';

function App() {
  const [ selectedTab, setSelectedTab ] = useState(0);

  function activate(i: number) {
    return () => {
      setSelectedTab(i);
    }
  }

  const tabs = [
    { name: 'List from', render: <From/>},
    { name: 'List to', render: <To/>},
    { name: 'Find fusion path', render: <PathFinder/>}
  ]

  return (
    <div className="App">
      {tabs.map((tab, i) => <Tab name={tab.name} active={selectedTab === i} handleActivation={activate(i)}>{tab.render}</Tab>)}
    </div>
  );
}

export default App;
