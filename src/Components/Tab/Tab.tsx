import React, { ReactElement } from 'react';
import './Tab.css';

function Tab(props: { active: boolean, name: string, handleActivation: () => void, children: ReactElement}) {
    if (!props.active) {
        return <div className='tab inactive'><button onClick={props.handleActivation}>{props.name}</button></div>
    }
    return <div className='tab active'>{props.children}</div>;
}

export default Tab;