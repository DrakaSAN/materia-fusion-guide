import React, { useState } from 'react';
import MateriaSelector from '../MateriaSelector/MateriaSelector';
import { listFusionFrom } from '../../core/fusions';
import { Materia, materiaNames } from '../../core/materia';

function From() {
    const [selectedMateria, setSelectedMateria] = useState<Materia>(materiaNames[0])

    return <div>
        <MateriaSelector onSelection={setSelectedMateria}></MateriaSelector>
        <ul>
            {listFusionFrom(selectedMateria).map((fusion) => {
                return <li>{fusion.from.join(' + ')} to make {fusion.result}</li>
            })}
        </ul>
    </div>
}

export default From;