import React from 'react';
import { Materia, materiaNames } from '../../core/materia';

function MateriaSelector(props: { defaultSelection?: Materia, onSelection: (materia: Materia) => void }) {
    return <select onChange={(event) => props.onSelection(event.target.value as Materia)}>
        {materiaNames.map((materia) => {
            return <option selected={materia === props.defaultSelection}>{materia}</option>
        })}
    </select>
}

export default MateriaSelector;