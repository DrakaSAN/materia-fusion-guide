import React, { useState } from 'react';
import MateriaSelector from '../MateriaSelector/MateriaSelector';
import { listFusionResultingIn } from '../../core/fusions';
import { Materia, materiaNames } from '../../core/materia';

function From() {
    const [selectedMateria, setSelectedMateria] = useState<Materia>(materiaNames[0])

    return <div>
        <MateriaSelector onSelection={setSelectedMateria}></MateriaSelector>
        <ul>
            {listFusionResultingIn(selectedMateria).map((fusion) => {
                return <li>{fusion.from.join(' + ')} to make {fusion.result}</li>
            })}
        </ul>
    </div>
}

export default From;