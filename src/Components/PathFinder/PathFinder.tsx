import React, { useState } from 'react';
import MateriaSelector from '../MateriaSelector/MateriaSelector';
import { findFusionPath } from '../../core/fusions';

function PathFinder() {
    const [from, setFrom] = useState('Fire');
    const [to, setTo] = useState('Blizzard');

    return <div>
        From: <MateriaSelector onSelection={setFrom}></MateriaSelector>
        To: <MateriaSelector onSelection={setTo} defaultSelection='Blizzard'></MateriaSelector>

        <ul>
            {findFusionPath(from, to).map((possiblePaths) => {
                return <li>
                    <ol>
                        {possiblePaths.map((fusion) => {
                            return <li>{`${fusion.from.join(' + ')} to get ${fusion.result}`}</li>;
                        })}
                    </ol>
                </li>
            })}
        </ul>
    </div>
}

export default PathFinder;